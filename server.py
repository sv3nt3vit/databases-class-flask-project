#!/usr/bin/env python2.7

"""
Columbia's COMS W4111.001 Introduction to Databases
"""

import datetime
import requests
import bcrypt
import os
import csv
import datetime
from datetime import timedelta
from sqlalchemy import *
from sqlalchemy.pool import NullPool
from flask import Flask, request, render_template, g, redirect, Response, session, url_for

from utils.db_helper import get_conn, query_db, get_query_list

tmpl_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')
app = Flask(__name__, template_folder=tmpl_dir)

APIKEY = apikey = "YOUR API KEY"

@app.route('/')
@app.route('/home', methods=['GET'])
def home():
  print(session)
  if not session.get('logged_in'):
    return render_template("login.html")
  else:
    return index()
  #return index()

@app.route('/login', methods=['POST'])
def do_admin_login():
    query = get_query_list()
    cur = get_conn()
    data,colnames = query_db(query['get_users_and_pw_hashes'])
    data_dict = {k:v for (k,v) in data}

    entered_un = request.form['username'].encode('utf-8')
    uname = request.form.get('username')
    entered_pw = request.form['password'].encode('utf-8')
    is_signup =  request.form.get('signupbtn')
    is_login =  request.form.get('loginbtn')
    if is_login:
        try:
          if bcrypt.checkpw(entered_pw, data_dict[entered_un]):
              session['username'] = entered_un
              session['logged_in'] = True
          else:
              print('Wrong password')
        except KeyError as e:
          print('No such username')
    elif is_signup:
        dt = datetime.datetime.now()
        hashedpw = bcrypt.hashpw(entered_pw, bcrypt.gensalt())
        session['username'] = entered_un
        session['logged_in'] = True
        cur.execute("INSERT INTO users(username, registration_date, pwhash) VALUES (%s, %s, %s);", (uname, dt.date(), hashedpw))
    return home()

@app.route('/logout')
def logout():
  session['logged_in'] = False
  return home()

@app.route('/submit_malware', methods=['POST'])
def submit_malware():
    print("FORM {}".format(request.form))
    resource = request.form.get('md5') # hash of file
    params = {'apikey' : APIKEY, 'resource' : resource}
    headers = {
        "Accept-Encoding": "gzip, deflate",
        "User-Agent" : "gzip,  My Python requests library example client or username"
        }
    response = requests.get('https://www.virustotal.com/vtapi/v2/file/report', params=params, headers=headers)
    template_name = 'index.html'
    if response.status_code == 200:
        json_response = response.json()
        if json_response['response_code'] == 1: # success
            query = get_query_list()
            cur = get_conn()

            scan_date = json_response['scan_date'] # scan date of file
            scan_id = json_response['scan_id'] # scan id of file
            positives = json_response['positives'] # positive tests of file

            query_db(query['insert_new_malware'] % (resource, scan_date, scan_id, int(positives)))
            query_db(query['insert_submits_malware'] % (resource, session['username']))
            template_name = "index_report.html"
            return render_template(template_name, report=json_response)
    return home()


def index():
  query = get_query_list()
  cur = get_conn()
  data,colnames = query_db(query['get_all_submitted_malware'])
  return render_template("index.html", malware=data[:10]) #cap to 10

@app.route('/users')
def users():
  query = get_query_list()
  cur = get_conn()
  data,colnames = query_db(query['get_most_active_users'])
  return render_template("users.html", data=data)

@app.route('/charts')
def charts():
  query = get_query_list()
  cur = get_conn()
  data2,colnames = query_db(query['get_most_active_countries'])
  data3,colnames = query_db(query['get_malware_submission_count'])
  data4,colnames = query_db(query['get_all_inet_malware'])
  submission_counts,colnames = query_db(query['get_submission_counts_per_day_top_10'])
  earliest_day,colnames = query_db(query['get_earliest_day'])
  malware_list = list(set([x[0] for x in submission_counts]))
  submissions_dict = {(x[0], x[1]): x[2] for x in submission_counts}
  data5 = generate_data_rows(malware_list, submissions_dict, earliest_day[0][0])
  return render_template("charts.html", data=data2, submissions=data3, domains=data4, submission_counts=data5)


def generate_data_rows(malware_list, submissions_per_day, earliest_day):
  rows = []
  rows.append(["Day"] + malware_list)
  date = earliest_day
  while (date.date() != datetime.datetime.now().date()):
    row_to_add = [str(date.date())]
    for malware in malware_list:
      try:
        submission_count = submissions_per_day[(malware, date)]
        row_to_add.append(submission_count)
      except KeyError:
        row_to_add.append(None)
    rows.append(row_to_add)
    date += timedelta(days=1)
  return rows

# Example of adding new data to the database
@app.route('/add', methods=['POST'])
def add():
  name = request.form['name']
  g.conn.execute('INSERT INTO test VALUES (NULL, ?)', name)
  return redirect('/')


if __name__ == "__main__":
  import click

  @click.command()
  @click.option('--debug', is_flag=True)
  @click.option('--threaded', is_flag=True)
  @click.argument('HOST', default='0.0.0.0')
  @click.argument('PORT', default=8111, type=int)
  def run(debug, threaded, host, port):
    HOST, PORT = host, port
    print ("running on %s:%d" % (HOST, PORT))
    app.secret_key = os.urandom(12)
    app.run(host=HOST, port=PORT, debug=debug, threaded=threaded)


  run()
