# VT-Ninja, a DB course project
NOTE: this repo contains sample code to project for the COMS W4111 Intro to DB course at Columbia University, but is not identical to the original code for privacy reasons.
The implementation, SQL schema and other vulnerable details may have been stripped, obscured or changed.
### [Sample Demo](http://35.196.72.81:8111 "VT-Ninja") ###
Proposed project name: Visualization of the daily malware landscape. 

The objective of this project is to satisfy one of the course�s requirements, develop an application that uses data stored in a database created by students, and which meets the project�s requirements.
We propose to use Virus Total (VT) private API to collect daily reports for submitted malware pieces and malicious URLs to build a full-stack application using Python Flask micro web framework.

The project aimes to visualize daily trends for malware and malicious url's submitted to VT.
VirusTotal aggregates many antivirus products and online scan engines to check for viruses that the user's own antivirus may have missed, or to verify against any false positives.

We would like to give an overview of what types of malware families are deployed in the wild on a given day or selected time interval, what are their characteristics, possibly do some basic aggregation and clustering based on the dynamic analysis features included in the VT report. 

Types of data we are looking for are daily reports for analyzed samples and urls, data that the VT was able to generate using its detection and sandbox engines.

We have obtained a private API key from VT for the purpose of this academic project. This will allow to get access to endpoints that provide reports for a given file (hash) or urls (that are otherwise not available with a free API version), or just most recent reports in a given time interval that were submitted to the VT by other users. This could give us the ability to visually identify current malware trends and possibly relate them to recent news. If there is no endpoint that can consistently return the most recent reports for submitted malware runs by other users, we would need to find a source to get new samples on a daily basis from another portal, generate a hash and then query VT /file/report endpoint.
