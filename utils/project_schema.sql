CREATE SCHEMA vt_;

CREATE TABLE malware ( 
	md5                  varchar(32)  NOT NULL  ,
	sha1                 varchar(40)   DEFAULT 'NULL' ,
	sha256               varchar(64)   DEFAULT 'NULL' ,
	scan_date            date  NOT NULL  ,
	positives            int   DEFAULT 0 ,
	scans                text []    ,
	scan_id              varchar(256)  NOT NULL  ,
	CONSTRAINT pk_malware PRIMARY KEY ( md5 )
 ) engine=InnoDB;

ALTER TABLE malware COMMENT 'Piece of malware (executable) with its attributes from VT report';

ALTER TABLE malware MODIFY md5 varchar(32)  NOT NULL   COMMENT 'MD5 hash of malware executable';

ALTER TABLE malware MODIFY sha1 varchar(40)   DEFAULT 'NULL'  COMMENT 'sha1 hash for malware executable';

ALTER TABLE malware MODIFY sha256 varchar(64)   DEFAULT 'NULL'  COMMENT 'SHA256 hash for malware executable';

ALTER TABLE malware MODIFY scan_date date  NOT NULL   COMMENT 'Date when malware piece was submitted to and analyzed by VT engines';

ALTER TABLE malware MODIFY positives int   DEFAULT 0  COMMENT 'Number of Aniti Virus engines in VT that detected this malware piece as malicious. This number can be 40-100 AVs.';

ALTER TABLE malware MODIFY scans text []     COMMENT 'Array of json';

ALTER TABLE malware MODIFY scan_id varchar(256)  NOT NULL   COMMENT 'Internal id given to a file by VT engine at submission time. Unique. One can retrieve report using this id later.';

CREATE TABLE metadata ( 
	md5                  varchar(32)  NOT NULL  ,
	codesize             varchar(16)  NOT NULL DEFAULT 'NULL' ,
	entrypoint           varchar(16)    ,
	filetype             varchar(16)  NOT NULL  ,
	filetype_extension   varchar(8)  NOT NULL  ,
	init_data_size       int    ,
	linker_version       varchar(8)    ,
	mime_type            varchar(32)    ,
	machine_type         varchar(32)    ,
	os_version           varchar(8)    ,
	pe_type              varchar(8)    ,
	subsystem            varchar(64)    ,
	subsystem_version    varchar(8)    ,
	timestamp            timestamp  NOT NULL  ,
	uninit_data_size     int    ,
	CONSTRAINT pk_metadata PRIMARY KEY ( md5 ),
	CONSTRAINT fk_metadata_malware FOREIGN KEY ( md5 ) REFERENCES malware( md5 ) ON DELETE CASCADE ON UPDATE CASCADE
 );

ALTER TABLE metadata COMMENT 'File executable metadate returned in a VT report for a submitted piece of malware';

ALTER TABLE metadata MODIFY md5 varchar(32)  NOT NULL   COMMENT 'primary key which is also a foreign key -> ref malware piece';

ALTER TABLE metadata MODIFY entrypoint varchar(16)     COMMENT 'Entrypoint (main) is the address in memory where instructions start executing';

ALTER TABLE metadata MODIFY filetype varchar(16)  NOT NULL   COMMENT 'Type of the file (architecture) : windows32 or osX or fike type such as pdf.';

ALTER TABLE metadata MODIFY filetype_extension varchar(8)  NOT NULL   COMMENT 'extension of the file';

ALTER TABLE metadata MODIFY init_data_size int     COMMENT 'Size of the data after modules initialization';

ALTER TABLE metadata MODIFY linker_version varchar(8)     COMMENT 'Version of the linker used';

ALTER TABLE metadata MODIFY machine_type varchar(32)     COMMENT 'Bit architecture of the underlying machine';

ALTER TABLE metadata MODIFY pe_type varchar(8)     COMMENT 'architecture of the portable executable';

ALTER TABLE metadata MODIFY subsystem varchar(64)     COMMENT 'interface type / gui / console';

CREATE TABLE modules_loaded ( 
	md5                  varchar(32)  NOT NULL  ,
	runtime_dlls         varchar(128) []    ,
	CONSTRAINT pk_metadata_4 PRIMARY KEY ( md5 ),
	CONSTRAINT fk_process_service_actions_0 FOREIGN KEY ( md5 ) REFERENCES malware( md5 ) ON DELETE CASCADE ON UPDATE CASCADE
 );

ALTER TABLE modules_loaded COMMENT 'loaded modules (e.g. dll''s)';

ALTER TABLE modules_loaded MODIFY md5 varchar(32)  NOT NULL   COMMENT 'primary key which is also a foreign key -> ref malware piece';

CREATE TABLE network_communication ( 
	md5                  varchar(32)  NOT NULL  ,
	dns_resolutions      varchar(256) []    ,
	tcp_communication    varchar(32) []    ,
	CONSTRAINT pk_metadata_0 PRIMARY KEY ( md5 ),
	CONSTRAINT fk_metadata_0_malware FOREIGN KEY ( md5 ) REFERENCES malware( md5 ) ON DELETE CASCADE ON UPDATE CASCADE
 );

ALTER TABLE network_communication COMMENT 'Network communicaiton that a piece of malware generates (if any)';

ALTER TABLE network_communication MODIFY md5 varchar(32)  NOT NULL   COMMENT 'primary key which is also a foreign key -> ref malware piece';

ALTER TABLE network_communication MODIFY dns_resolutions varchar(256) []     COMMENT 'URL''s that malware connects to';

CREATE TABLE process_service_actions ( 
	md5                  varchar(32)  NOT NULL  ,
	services_opened      varchar(64) []    ,
	processes_created    text []    ,
	service_managers_opened text []    ,
	CONSTRAINT pk_metadata_3 PRIMARY KEY ( md5 ),
	CONSTRAINT fk_file_system_actions_0 FOREIGN KEY ( md5 ) REFERENCES malware( md5 ) ON DELETE CASCADE ON UPDATE CASCADE
 );

ALTER TABLE process_service_actions COMMENT 'when executing the malware generated acitons wrt processes and services in the sandbox env.';

ALTER TABLE process_service_actions MODIFY md5 varchar(32)  NOT NULL   COMMENT 'primary key which is also a foreign key -> ref malware piece';

ALTER TABLE process_service_actions MODIFY services_opened varchar(64) []     COMMENT 'Services opened in the env during execution';

ALTER TABLE process_service_actions MODIFY processes_created text []     COMMENT 'Processes created during execution of malware in the env';

ALTER TABLE process_service_actions MODIFY service_managers_opened text []     COMMENT 'Opened service managers during execution of malware in the sandbox env';

CREATE TABLE sync_mechanism ( 
	md5                  varchar(32)  NOT NULL  ,
	mutexes_created      varchar(32) []    ,
	mutexes_opened       varchar(32) []    ,
	CONSTRAINT pk_metadata_2 PRIMARY KEY ( md5 ),
	CONSTRAINT fk_network_communication_0 FOREIGN KEY ( md5 ) REFERENCES malware( md5 ) ON DELETE CASCADE ON UPDATE CASCADE
 );

ALTER TABLE sync_mechanism COMMENT 'multiprocessing signature (mutexes etc.)';

ALTER TABLE sync_mechanism MODIFY md5 varchar(32)  NOT NULL   COMMENT 'primary key which is also a foreign key -> ref malware piece';

ALTER TABLE sync_mechanism MODIFY mutexes_created varchar(32) []     COMMENT 'Created mutexes during the runtime';

CREATE TABLE url ( 
	scan_date            date  NOT NULL  ,
	positives            int   DEFAULT 0 ,
	scans                text []    ,
	scan_id              varchar(256)  NOT NULL  ,
	url                  text  NOT NULL  ,
	CONSTRAINT pk_malware_0 PRIMARY KEY ( url )
 );

ALTER TABLE url COMMENT 'URL''s';

ALTER TABLE url MODIFY scan_date date  NOT NULL   COMMENT 'Date when malware piece was submitted to and analyzed by VT engines';

ALTER TABLE url MODIFY positives int   DEFAULT 0  COMMENT 'Number of Aniti Virus engines in VT that detected this malware piece as malicious. This number can be 40-100 AVs.';

ALTER TABLE url MODIFY scans text []     COMMENT 'Array of json';

ALTER TABLE url MODIFY scan_id varchar(256)  NOT NULL   COMMENT 'Internal id given to a file by VT engine at submission time. Unique. One can retrieve report using this id later.';

ALTER TABLE url MODIFY url text  NOT NULL   COMMENT 'URL under investigation (both malicious and non-malicious)';

CREATE TABLE Users ( 
	uid                  SERIAL,
	username             varchar(100)  NOT NULL  ,
	registration_date    date  NOT NULL  ,
	CONSTRAINT pk_user PRIMARY KEY ( uid )
 );

ALTER TABLE `user` COMMENT 'Entity to represent a registered user in the portal. Users can submit malware and urls and retrieve reports for the sumbitted pieces.';

ALTER TABLE `user` MODIFY uid int  NOT NULL  AUTO_INCREMENT COMMENT 'user id';



CREATE TABLE communicates_with ( 
	md5  varchar(32)  NOT NULL REFERENCES malware( md5 ) ON DELETE CASCADE ON UPDATE CASCADE ,
	url  text  NOT NULL  REFERENCES url( url ) ON DELETE CASCADE ON UPDATE CASCADE,
	PRIMARY KEY ( md5, url )
 );

CREATE INDEX idx_communicates_with_0 ON communicates_with ( md5 );

CREATE INDEX idx_communicates_with_1 ON communicates_with ( url );

ALTER TABLE communicates_with COMMENT 'malware can communicate with certain ip''s';



CREATE TABLE file_system_actions ( 
	md5                  varchar(32)  NOT NULL  ,
	files_opened         text []    ,
	files_read           text []    ,
	files_written        text []    ,
	PRIMARY KEY ( md5 ),
	FOREIGN KEY ( md5 ) REFERENCES malware( md5 ) ON DELETE CASCADE ON UPDATE CASCADE
 );

ALTER TABLE file_system_actions COMMENT 'trace on the file system that a piece of malware generates';

ALTER TABLE file_system_actions MODIFY md5 varchar(32)  NOT NULL   COMMENT 'primary key which is also a foreign key -> ref malware piece';

ALTER TABLE file_system_actions MODIFY files_opened text []     COMMENT 'Files opened on the filesystem';

CREATE TABLE submits_malware ( 
	uid                  int  NOT NULL  ,
	md5                  varchar(32)  NOT NULL  ,
	timestamp            datetime  NOT NULL  ,
	PRIMARY KEY ( uid, md5 ),
	FOREIGN KEY ( md5 ) REFERENCES malware( md5 ) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY ( uid ) REFERENCES Users( uid ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX idx_submits_url_0 ON submits_malware ( md5 );

CREATE INDEX idx_submits_url_1 ON submits_malware ( uid );

ALTER TABLE submits_malware COMMENT 'User entity can submit a malware executable to the portal';

CREATE TABLE submits_url ( 
	uid                  int  NOT NULL  ,
	url                  text  NOT NULL  ,
	timestamp            datetime  NOT NULL  ,
	PRIMARY KEY ( uid, url ),
	FOREIGN KEY ( url ) REFERENCES url( url ) ON DELETE CASCADE ON UPDATE CASCADE,
	FOREIGN KEY ( uid ) REFERENCES `user`( uid ) ON DELETE CASCADE ON UPDATE CASCADE
 );

CREATE INDEX idx_submits_url ON submits_url ( url );

CREATE INDEX idx_submits_url ON submits_url ( uid );

ALTER TABLE submits_url COMMENT 'User entity can submit a url to the portal';

