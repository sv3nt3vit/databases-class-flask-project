import csv
import json
import time
import os
import sys

import psycopg2
import psycopg2.extras

import requests

DB = 'projname'
UNAME = 'yourusername'
PSW = 'yourpassword'
HOST = 'host'

QUERY = {   "insert_new_malware": "INSERT INTO malware (md5, scan_date, scan_id, positives) \
                                    VALUES ('%s', '%s', '%s', '%d');",
            "insert_submits_malware": "INSERT INTO submits_malware (md5, uid)\
                                        SELECT '%s', uid \
                                        FROM users \
                                        WHERE username = '%s';",
            "get_users_and_pw_hashes": "SELECT username, pwhash \
                                        FROM users U",
            "get_most_active_users" :
                        "SELECT username, count(*) \
                         FROM users U \
                         INNER JOIN submits_malware S \
                         ON U.uid=S.uid \
                         GROUP BY U.username;",
            "get_all_submitted_malware" :
                         "SELECT S.md5, username \
                          FROM ( SELECT md5, max(timestamp) \
                                 FROM submits_malware \
                                 GROUP BY md5) as foo \
                          INNER JOIN submits_malware S \
                          ON foo.md5 = S.md5 \
                          AND foo.max = S.timestamp \
                          INNER JOIN users U on S.uid = U.uid;",
            "get_malware_submission_count" :
                          "SELECT M.md5, count(*) \
                          FROM malware M \
                          INNER JOIN submits_malware SM \
                          ON M.md5 = SM.md5 \
                          GROUP BY M.md5;",
            "get_all_inet_malware" :
                         "SELECT M.md5, dns_resolutions \
                          FROM malware M \
                          INNER JOIN network_communication N \
                          ON M.md5 = N.md5 \
                          EXCEPT ( \
                                  SELECT M.md5, dns_resolutions \
                                  FROM malware M \
                                  INNER JOIN network_communication N \
                                  ON M.md5 = N.md5 \
                                  WHERE dns_resolutions = '{}');",
            "get_most_active_countries" :
                        "SELECT country, count(*) \
                        FROM users U \
                        INNER JOIN submits_malware S \
                        ON U.uid=S.uid \
                        GROUP BY U.country;",
            "get_submission_counts_per_day" :
                        "SELECT md5, date_trunc, count(*) \
                        FROM ( SELECT md5, date_trunc('day', timestamp) \
                                FROM submits_malware) AS day_truncated \
                        GROUP BY md5, date_trunc \
                        ORDER BY date_trunc;",
            "get_earliest_day":
                        "SELECT date_trunc('day', min(timestamp)) \
                          FROM submits_malware;",
            "get_submission_counts_per_day_top_10":
                        "SELECT sub_counts.md5, sub_counts.date_trunc, sub_counts.count \
                        FROM ( SELECT md5, date_trunc, count(*) \
                                FROM ( SELECT md5, date_trunc('day', timestamp) \
                                        FROM submits_malware) AS day_truncated \
                                        GROUP BY md5, date_trunc \
                                        ORDER BY date_trunc) AS sub_counts \
                        INNER JOIN (SELECT md5, count(*) \
                                    FROM submits_malware \
                                    GROUP BY md5 \
                                    ORDER BY count DESC LIMIT 10) AS top10 \
                        ON sub_counts.md5 = top10.md5;"
        }


def get_query_list():
    return QUERY

def get_conn():
    params = {
        'database': DB,
        'user': UNAME,
        'password': PSW,
        'host': HOST,
        'port': 5432
    }
    try:
        conn = psycopg2.connect(**params)
        conn.set_session(autocommit=True)
        cursor = conn.cursor()
        return cursor
    except Exception as e:
        print(str(e))

def query_db(query):
    cursor = get_conn()
    if cursor:
        try:
            cursor.execute(query)
            data = cursor.fetchall()
            colnames = [desc[0] for desc in cursor.description]
            return (data,colnames)
        except Exception as e:
            print(str(e))
    else:
        print("query_db() no cursor, returning None")
        return None


